# import numpy as np
import cv2


cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    print(dir(frame))

    # Our operations on the frame come here
    # gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # Draw rectangle on image
    # rectangle = cv2.rectangle(frame, (0, 0), (50, 50), (255, 0, 0), 1)

    # Display the resulting frame
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
