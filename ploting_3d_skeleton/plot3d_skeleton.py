import json
import matplotlib.pyplot as plt


class Point3d:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


def load_posenet_data():
    skeletons = []
    with open('data/data.json', 'r') as data_file:
        data = json.load(data_file)
        for record in data['data']:
            points = list(record['xs'].values())
            point_len = len(points)
            skeleton = [Point3d(points[i], points[i+1], 0) for i in range(0, point_len, 2)]
            skeletons.append(skeleton)
    return skeletons


class plot3dClass:
    def __init__(self, fps):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.fps = fps
        self._setup_settings()

    def draw_posnet(self, skeleton, line_color='g', dot_color='r'):
        plt.cla()
        self._setup_settings()

        skeleton_len = len(skeleton)
        point = skeleton[0]

        # Draw dots
        self.ax.scatter(point.x, point.z, point.y, color=dot_color)  # draw a nose
        for i in range(5, skeleton_len):
            point = skeleton[i]
            self.ax.scatter(point.x, point.z, point.y, color=dot_color)

        # Draw lines between points
        self.ax.plot(
            [skeleton[6].x, skeleton[5].x],
            [skeleton[6].z, skeleton[5].z],
            [skeleton[6].y, skeleton[5].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[6].x, skeleton[8].x],
            [skeleton[6].z, skeleton[8].z],
            [skeleton[6].y, skeleton[8].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[10].x, skeleton[8].x],
            [skeleton[10].z, skeleton[8].z],
            [skeleton[10].y, skeleton[8].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[6].x, skeleton[12].x],
            [skeleton[6].z, skeleton[12].z],
            [skeleton[6].y, skeleton[12].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[11].x, skeleton[5].x],
            [skeleton[11].z, skeleton[5].z],
            [skeleton[11].y, skeleton[5].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[7].x, skeleton[5].x],
            [skeleton[7].z, skeleton[5].z],
            [skeleton[7].y, skeleton[5].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[7].x, skeleton[9].x],
            [skeleton[7].z, skeleton[9].z],
            [skeleton[7].y, skeleton[9].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[11].x, skeleton[12].x],
            [skeleton[11].z, skeleton[12].z],
            [skeleton[11].y, skeleton[12].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[11].x, skeleton[14].x],
            [skeleton[11].z, skeleton[14].z],
            [skeleton[11].y, skeleton[14].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[15].x, skeleton[14].x],
            [skeleton[15].z, skeleton[14].z],
            [skeleton[15].y, skeleton[14].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[13].x, skeleton[12].x],
            [skeleton[13].z, skeleton[12].z],
            [skeleton[13].y, skeleton[12].y],
            color=line_color
        )
        self.ax.plot(
            [skeleton[13].x, skeleton[16].x],
            [skeleton[13].z, skeleton[16].z],
            [skeleton[13].y, skeleton[16].y],
            color=line_color
        )

        # Redraw plot
        plt.draw()
        plt.pause(1/self.fps)

    def _setup_settings(self):
        """Setup plot settings as labeles, axis formaters and metrics"""
        self.ax.set_xlim(600, 0)
        self.ax.set_zlim(600, 0)
        self.ax.xaxis.set_major_formatter(plt.NullFormatter())
        self.ax.yaxis.set_major_formatter(plt.NullFormatter())
        self.ax.zaxis.set_major_formatter(plt.NullFormatter())
        self.ax.set_xlabel('X')
        self.ax.set_zlabel('Y')
        self.ax.set_ylabel('Z')


def main():
    plot_3d = plot3dClass(fps=1)
    skeletons = load_posenet_data()
    for skeleton in skeletons:
        plot_3d.draw_posnet(skeleton)


if __name__ == "__main__":
    main()
