# Analiza przysiadu z wykorzystaniem szacowania póz 3D (Pose estimation 3D squat)

### Opis
Celem pracy jest przygotowanie modelu uczenia maszynowego, który na podstawie obrazu z kamery w czasie rzeczywistym będzie przenosił ludzką sylwetkę na wykres 3D, na którym pojawi się szkielet postaci tj. 15(17) punktów kluczowych (w zależności od wybranych danych uczących) połączonych ze sobą.

Następnie zostanie przeprowadzona analiza wykonywania przysiadu ograniczona do sprawdzenia poprawnego nachylenia tułowia podczas wykonywania ćwiczenia. Odpowiednie informację będą wyświetlane na ekranie.

Ze względu na dodatkowe możliwości sprzętowe zostaną również porównane czasy uczenia modelu na różnych procesorach(**i5-8250U**, **Ryzen 5 3600**) oraz karcie graficznej (**Radeon RX5600XT**). Oprócz porównania czasu tworzenia modeli uczenia maszynowego zostanie przeanalizowana wydajność analizy obrazu z kamery w czasie rzeczywistym (FPS) w zależności od wykorzystanego sprzętu.

### Kroki tworzenia pracy inżynierskiej
1. Rozpoznanie bibliotek do tworzenia wykresów 3D
1. Rozpoznanie bibliotek do przechwytywania obrazu z kamery laptopa lub z zewnętrznie podłączonego urządzenia (telefon, aparat, kamera).
1. Przegląd udostępnionych materiałów/dokumentów na temat architektur modeli szacowania póz 3D.
1. Przegląd zbiorów danych do rozpoznawania póz 3D.
1. Utworzenie modułu python do rysowania szkieletu człowieka na wykresie 3D.
1. Utworzenie modułu python do przechwytywania obrazu z kamery inernetowej w laptopie lub z zewnętrznego sprzętu.
1. Wybór zbioru danych uczących oraz odpowiednie przygotowanie danych.
1. Wybór architektury sieci oraz implementacja modelu.
1. Dostosowanie parametrów modelu.
1. Spięcie 2 utworzonych modułów oraz modelu.
1. Implementacja obliczenia kątów pochylenia.
1. Testowanie dotychczasowej pracy oraz wprowadzanie poprawek optymalizacyjnych.
1. Przygotowanie części merytorycznej do pracy inżynierskiej w LaTeX'u.

** Na bieżąco zdobywanie wiedzy na temat uczenia maszynowego - poradniki, tutoriale w internecie oraz wiedzy z przedmiotu Wstęp do sieci neuronowych

### Stos technologiczny

### Podejścia do szacowania pozycji człowieka 3D
1. Wytrenować model, który od razu wyznaczy współrzędne 3D z dostarczonego obrazu np. [EpipolarPose](https://arxiv.org/abs/1903.02330)
1. Wykryć punkty 2D i przekształcić je na punkty 3D.

### Wymagania w zależności od platformy (Linux, Windows)
* openCV
* matplotlib

### Zbiory danych
* [MPII Human Pose Dataset](http://human-pose.mpi-inf.mpg.de/) - zbiór 25 000 obrazów przedstawiających ponad 40 000 osób wraz z opisanymi stawami ciała. Obrazy były zbierane przy użyciu ustalonej taksonomii codziennej człowieka. Wyodrębnionych zostało 410 aktywności, a każdy obraz opatrzony jest etykietą działania. Obrazy są wyodrębnione z filmów ze strony YouTube oraz opatrzone poprzedzającymi i następującymi klatkami bez adnotacji. ~ 13GB

### Bibliografia
* [Showing pose 3D](https://github.com/markjay4k/3D-Pose-Estimation)
* [Ciekawa prezentacja na temat pose estimation](http://www.cs.cornell.edu/courses/cs6670/2018fa/lec18-pose.pdf)
* [Obszerny materiał na temat różnych problemów w pozach 3D](https://www.kdnuggets.com/2020/08/3d-human-pose-estimation-experiments-analysis.html)
* [Human 3D pose estimation guide](https://nanonets.com/blog/human-pose-estimation-3d-guide/)

### Ciekawe repozytoria
* [ViedoPose3D](https://github.com/facebookresearch/VideoPose3D)
* [EpipolarPose](https://github.com/mkocabas/EpipolarPose)
* [PoseNet Python](https://github.com/rwightman/posenet-python)
* [openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
* [3D-Pose-Estimation](https://github.com/markjay4k/3D-Pose-Estimation) - powiązane [viedo](https://www.youtube.com/watch?v=nUjGLjOmF7o)